var yml = require('js-yaml');
var all_rules = null;

require('assert').doesNotThrow(
	function() {
		all_rules = require(process.cwd() + '/rules.yml');
	},
	function(err) {
		console.error('There\'s no rules.yml file!');
		console.error('Please download rules.yml from https://github.com/rocsci/petrovich/blob/master/lib/petrovich/rules.yml');
	}
);

function inflect(name, gcase, rules, gender) {
	var _gender = gender ? ((['male', 'female', 'androgynous'].indexOf(gender.toLowerCase()) < 0) ? 'androgynous' : gender.toLowerCase()) : 'androgynous';
	return name.split('-').map(function(value, index, array) {
		return find_and_apply(value, gcase, {
			first_word: ((index == 0) && (array.length > 1))
		}, rules, _gender);
	}).join('-');
}

function find_and_apply(name, gcase, features, rules, gender) {
	var rule = find_for(name, features, rules, gender);
	return (rule) ? apply(name, gcase, rule) : name;
}

function find_for(name, features, rules, gender) {

	var tags = extract_tags(features);

	if (rules.exception !== undefined) {
		var match = find(name, rules.exception, true, tags, gender);
		if (match)
			return match;
	}

	return find(name, rules.suffixes, false, tags, gender);
}

function find(name, rules, match_whole_word, tags, gender) {
	for (var i = 0; i < rules.length; i++)
		if (match(name, rules[i], match_whole_word, tags, gender))
			return rules[i];
}

function match(name, rule, match_whole_word, tags, gender) {
	if (!tags_allow(tags, rule.tags))
		return false;
	if (((rule.gender == 'male') && (gender == 'female')) || ((rule.gender == 'female') && (gender == 'male')))
		return false;

	var name_l = name.toLowerCase();
	return rule.test.some(function(value) {
		var test = match_whole_word ? name_l : name_l.slice(-1 * value.length);
		return (test === value);
	})
}

function apply(name, gcase, rule) {

	var sw = {
		'.': function(name) {
			return name
		},
		'-': function(name) {
			return name.substr(0, name.length - 1)
		}
	};

	modificator_for(gcase, rule).split('').forEach(function(char) {
		name = (sw[char]) ? sw[char](name) : (name + char);
	})

	return name;
}

function extract_tags(features) {
	var ret = [];
	for (var i in features)
		if (features[i] === true)
			ret.push(i);
	return ret;
}

function modificator_for(gcase, rule) {
	var mod = {
		'nominative': '.',
		'genitive': rule.mods[0],
		'dative': rule.mods[1],
		'accusative': rule.mods[2],
		'instrumental': rule.mods[3],
		'prepositional': rule.mods[4]
	}[gcase];
	return (mod) ? mod : '.';
}

function tags_allow(tags, rule_tags) {
	var rule_tags = (!rule_tags) ? [] : rule_tags;
	return (rule_tags.filter(function(value) {
		return (tags.indexOf(value) >= 0) ? undefined : value
	}).length == 0)
}

var Petrovich = function(gender, gcase) {
	this._final = [];
	this._gender = gender;
	this._gcase = gcase;

	if ((typeof gender !== "string") && (typeof gender === "object")) {
		this._gender = gender.gender;
		this._gcase = gender.gcase;
	}

}

Petrovich.prototype.lastname = function(lastname) {
	this._final.push(inflect(lastname, this._gcase, all_rules.lastname, this._gender));
	return this;
}

Petrovich.prototype.firstname = function(firstname) {
	this._final.push(inflect(firstname, this._gcase, all_rules.firstname, this._gender));
	return this;
}

Petrovich.prototype.middlename = function(middlename) {
	this._final.push(inflect(middlename, this._gcase, all_rules.middlename, this._gender));
	return this;
}

Petrovich.prototype.gender = function(gender) {
	this._gender = gender;
	return this;
}

Petrovich.prototype.gcase = function(gcase) {
	this._gcase = gcase;
	return this;
}

Petrovich.prototype.toString = function(save) {
	var name = this._final.join(' ');
	if (!save)
		this._final = [];
	return name;
}

Petrovich.prototype.exec = function(save) {
	return this.toString(save);
}

Petrovich.gcases = {
	NOMINATIVE: 'nominative',
	N: 'nominative',
	GENITIVE: 'genitive',
	G: 'genitive',
	DATIVE: 'dative',
	D: 'dative',
	ACCUSATIVE: 'accusative',
	A: 'accusative',
	INSTRUMENTAL: 'instrumental',
	I: 'instrumental',
	PREPOSITIONAL: 'prepositional',
	P: 'prepositional'
}

Petrovich.genders = {
	MALE: 'male',
	M: 'male',
	FEMALE: 'female',
	F: 'female',
	ANDROGYNOUS: 'androgynous',
	A: 'androgynous'
}

Petrovich.lastname = function(name, gender, gcase) {
	return inflect(name, gcase, all_rules.lastname, gender);
}
Petrovich.firstname = function(name, gender, gcase) {
	return inflect(name, gcase, all_rules.firstname, gender);
}
Petrovich.middlename = function(name, gender, gcase) {
	return inflect(name, gcase, all_rules.middlename, gender);
}

module.exports = Petrovich;