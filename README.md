![Petrovich](http://petrovich.rocketscience.it/logo.png)
# Petrovich-js
Склонение русских имен, фамилий, и отчеств.  
Портировано с библиотеки от авторов Андрей Козлов, Дмитрий Усталов:  
> https://github.com/rocsci/petrovich  

## Установка
В папке с проектом:
> npm install petrovich-js

Для работы библиотеки нужны правила склонений. Скачать rules.yml можно из репозитория с оригинальной библиотекой
> https://github.com/rocsci/petrovich/blob/master/lib/petrovich/rules.yml

Добавить библиотеку в проект:  
> var Petrovich = require('petrovich');

## Использование
### Константы для склонения и пола:
Склонения:  
> 
Petrovich.gcases.N == Petrovich.gcases.NOMINATIVE == 'nominative'  
Petrovich.gcases.G == Petrovich.gcases.GENITIVE == 'genitive'  
Petrovich.gcases.D == Petrovich.gcases.DATIVE == 'dative'  
Petrovich.gcases.A == Petrovich.gcases.ACCUSATIVE == 'accusative'  
Petrovich.gcases.I == Petrovich.gcases.INSTRUMENTAL == 'instrumental'  
Petrovich.gcases.P == Petrovich.gcases.PREPOSITIONAL == 'prepositional'

Пол:
> 
Petrovich.genders.M == Petrovich.genders.MALE == 'male'  
Petrovich.genders.F == Petrovich.genders.FEMALE == 'female'  
Petrovich.genders.A == Petrovich.genders.ANDROGYNOUS == 'androgynous'  

### Создание Petrovich объекта:
> 
var name = new Petrovich('male', Petrovich.gcases.D);  
var name = new Petrovich({gcase: 'dative', gender: Petrovich.genders.MALE});  
var name = new Petrovich();  

Параметры по умолчанию: androgynous, nominative

### Склонение
Вызовы lastname, firstname, middlename добавляют результат во внутренний кэш в порядке их вызова. Цепные вызовы:
> 
name.lastname('Иванов').toString();  
name.gcase(Petrovich.gcases.INSTRUMENTAL).lastname('Иванов').firstname('Иван').toString();  
name.gcase('genitive').gender('female').lastname('Иванова').firstname('Маруся').middlename('Петровна').toString();  
name.gcase('genitive').gender('female').firstname('Маруся').lastname('Иванова').middlename('Петровна').toString();

Возможно также такое использование без цепных вызовов:
> 
name.gcase('dative');  
name.lastname('Иванов');  
name.firstname('Иван');  
name.toString();  

При преобразовании в строку (toString вызывается вручную, либо автоматически при конкатенации строк) кэш очищается. Для возможности многократного использования в toString вручную нужно передать "true":
> 
name.toString(true);  
name.toString(true);  

Возможно простое использование без объекта, результатом является сразу строка:
> 
var Petrovich = require('petrovich-js');  
Petrovich.lastname('Иванов', 'male', 'dative');  
Petrovich.firstname('Иван', 'male', 'dative');  
Petrovich.middlename('Петрович', 'male', 'dative');  